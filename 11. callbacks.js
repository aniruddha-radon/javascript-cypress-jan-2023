
const greetAdmin = () => 'Hey';
greetAdmin(); // 'Hey'

const greetEmployee = () => 'Hello';
greetEmployee(); // 'Hello'

// Single Responsibility
const greetUser = (fn) => {
    // fn will have a reference to a function
    // fn is a callback
    const message = fn(); // calling the reference
    console.log(message);
}

const loggedInUser = 'Admin';

if(loggedInUser === 'Admin') {
    // passing the reference of the greetAdmin fn
    // NOT INVOKING IT
    greetUser(greetAdmin)
} else if (loggedInUser === 'Employee') {
    greetUser(greetEmployee);
}

// a function that is passed as a parameter
// is called a callback

const numberModifier = (number, callback) => {
    // number = 5
    // callback(number) - calling the callback
    // callback references = n => n ** 2
    // callback is a reference to a function
    const modifiedNumber = callback(number);
    return modifiedNumber;
}

numberModifier(5, n => n + 5); // 5
numberModifier(5, n => n ** 2); // 25



const names = ["a", "b", "c"];


const toUppercase = (str) => str.toUpperCase();


names.map(toUppercase);

const map = (array, callback) => {
    // array = [1, 2, 3, 4, 5]
    // callback = n => n - 1

    for(let element of array) {
        // element = 2
        const result = callback(element);
        console.log(result);
    }
}

map([1, 2, 3, 4, 5], n => n - 1);

