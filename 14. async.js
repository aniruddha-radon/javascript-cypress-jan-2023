const five = () => console.log('five');

const four = () => {
    five();
    console.log('four');
}

const three = () => {
    four();
    console.log('three');
}

const two = () => {
    three();
    console.log('two');
}

const one = () => {
    two();
    console.log('one');
}


console.log('first');

// async
setTimeout(() => {
    console.log('second');
}, 3000);

console.log('last');

for (let count = 0; count < 10000; count++) {
    console.log(count);
}


// with a promise
// Promise is an built-in class
// create an object of the Promise class

const promiseConstructorCallback = (resolve, reject) => {
    // resolve and reject are callbacks and we can invoke them
    resolve({ name: 'darjeeling tea' });
    // when resolve is called, it means everything was successful
    // fulfills the promise

    reject({ message: 'no milk' });
    // it means that something failed
    // rejects the promise
}

const promiseObj = new Promise(promiseConstructorCallback);

promiseObj.then(data => {
    console.log(data);
    console.log('excellent tea');
}).catch(err => {
    console.log('no tea for narayan');
});

// new Promise();
// when promise is made -> the status is pending
// this calls the constructor of the Promise class
// constructor fn takes a callback
// the callback takes two parameters = resolve and reject
// resolve is a callback fn
// reject is a callback fn
// { key: value pairs that promise class defines }
// 1. then -> holds a function
// 2. catch -> holds a function
// 3. finally -> holds a function
// { then: function, catch: function, finally: function }


const userPromise = fetch("https://jsonplaceholder.typicode.com/users");
// userPromise contains an object of the Promise class

userPromise.then(response => {
    // waiting for the first response (might not contain data)
    console.log("FIRST THEN BLOCK", response);

    // it waits for the entire data to come
    // it will convert the stringified json to a usable javascript type
    return response.json();

    // if you return anything from the then block, it will be sent to the 
    // next then block after the promise resolves
}).then(data => {
    console.log("SECOND THEN BLOCK", data)
}).catch(error => {
    console.log(error)
});

// https://jsonplaceholder.typicode.com/posts

fetch("https://jsonplaceholder.typicode.com/posts").then(response => {
    return response.json();
}).then(posts => {
    const filteredPosts = posts.filter(post => post.length > 30);
    // for(let post of posts) {
    //     if(post.title.length > 30) {
    //         filteredPosts.push(post);
    //     }
    // }

    console.log(filteredPosts);
}).catch(error => {
    console.log(error, "SOMETHING WENT WRONG");
})


// async await
// must always use a function to use async await
const getPosts = async () => {
    try {
        const response = await fetch("https://jsonplaceholder.typicode.com/posts");
        const posts = await response.json();
        const filteredPosts = posts.filter(post => post.length > 30);
        console.log(filteredPosts);
    } catch (e) {
        console.log(e);
    }
}


// <div class="commercial">
//<div id="coditas-one"></div>
// <div id="tcs"></div>
// document object model -> DOM -> is stored in a variable called as document
const div = {
    tagName: "div",
    id: "",
    className: "commercial",
    children: [
        { tagName: "div", id: "coditas-one", className: "" },
        { tagName: "div", id: "tcs", className: "" },
    ]
}








const promise = new Promise((resolve, reject) => {
    resolve("chocolates");

    reject("no chocolates");
});

promise.then((response) => {
    console.log(response);
}).catch(error => {
    console.log(error);
})