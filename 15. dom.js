document

// getElementById
document.getElementById("coditas-one");
// we get the reference of the object with the id "coditas-one"
// from the document variable

// querySelector 
document.querySelector("#coditas-one");
// we get the reference of the object with the id "coditas-one"
// from the document variable

// getElementsByClassName
// gives out a LIST of elements not one object
document.getElementsByClassName("commercial");
// all element references where the class is commercial
// are stored in list and list returned

document.querySelector(".commercial");
// we get the reference of the first object with the class "commercial"
// from the document variable

document.querySelectorAll(".commercial");
// all element references where the class is commercial
// are stored in list and list returned