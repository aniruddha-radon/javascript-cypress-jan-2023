const sentence = 'this';
const friends = [{ name: 'a' }, { name: 'b' }];


// 1st. simple for loop
//  initialization; execute condition;      post execution statement
for(let index = 0;  index < sentence.length; index++) {
    // index = 4         
    const char = sentence[index];
    console.log(char);
}

for(let index = 0; index < friends.length; index++) {
    const friend = friends[index];
    console.log(friend);
}

const friend = {
    name: 'aniruddha',
    age: 29
};

const friendKeys = Object.keys(friend); // returns an array of all the keys in the object
// ['name', 'age'];
for(let index = 0; index < friendKeys.length; index++) {
    // index = 2
    const key = friendKeys[index];
    // key = 'age'

    const value = friend[key];
    // value = 29

    console.log(value);
}

// for in loop
// populating index will now be done by JS
for(let index in sentence) {
    const char = sentence[index];
    console.log(char);
}

for(let index in friends) {
    const friend = friends[index];
    console.log(friend);
}

for(let key in friend) {
    const value = friend[key];
    console.log(value);
}

// for of loop
// DOES NOT WORK ON OBJECTS

for(let char of sentence) {
    console.log(char);
}

for(let friend of friends) {
    console.log(friend);
}

// break -> it breaks out of the loop
// continue -> it skips the current iteration and continues from the next


const people = [
    { name: 'a', age: 31 },
    { name: 'b', age: 27 },
    { name: 'c', age: 24 },
    { name: 'd', age: 40 },
];

for(let person of people) {
    // person = { name: 'c', age: 24 }
    if(person.age < 25) {
        break;
    }

    console.log(person);
}

