// operators and the Math class methods

const num1 = 10;
const num2 = 2;

// addition +
num1 + num2; // 12;
const addition = num1 + num1;
const sum = num1 + num2;

// subtraction -
num1 - num2; // 8
const difference = num1 - num2;

// multiplication
num1 * num2; // 20
const product = num1 * num2;

// division
num1 / num2; // 5
10 / 3; // 3.333
const division = num1 / num2;

// remainder (mod) %
const remainder = num1 % num2; // 0

// power (raised to) **
const power = num1 ** num2; // 100

// NaN = Not a Number
NaN; // typeof NaN == 'number'

// Math
// Math.methodName

Math.round(4.7); // 5;
Math.round(4.4); // 4;

// nearest intger greater than current number
Math.ceil(4.1); // 5
Math.ceil(4.9); // 5

Math.floor(4.1); // 4
Math.floor(4.9); // 4

Math.random(); // random number between 0 and 1
// excludes 0 and 1

// 0.0006 * 10 = Math.ceil(0.006) = 1
// 0.99 * 10 = Math.ceil(9.9) = 10
 