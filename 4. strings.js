const greet = 'Hello';
const name = 'Aniruddha';

// join/merge/concatenate
// old way (not recommended) +

let fullGreeting = greet + ", " + name + "."; 
// Hello, Aniruddha.

// new way (use this)
// es6+, ``
fullGreeting = `${greet}, ${name}.`;

// properties
// positions  123456
let word = "oddity";
// word = "abcd";
// index      012345

word.length; // 6

// accessing characters from a string
word[2]; // d
word[100]; // undefined

// change character at index
const anotherWord = 'pose';
anotherWord[0]; // p
anotherWord[0] = 'n'; // this does NOT change the letter
// strings are immutable

// string methods
anotherWord.replace('p', 'n');
// creates a NEW string with the replacements


const csvRow = 'aniruddha,30,anirudha@gmail.com';
csvRow.split(','); // ['aniruddha', '30', 'anirudha@gmail.com'];

csvRow.includes('30'); // true
csvRow.includes('lkdsjflkdsf'); // false