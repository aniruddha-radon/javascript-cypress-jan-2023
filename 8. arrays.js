const numbers = [10, 20, 30, 40, 50]; 
// index         0   1   2   3   4

// accessing
numbers[4]; // 50
numbers[50]; // undefined

const index = 0;
numbers[index]; // 10

// modify at index
// numbers[4] = 60;
// [10, 20, 30, 40, 60];

// properties
const length = numbers.length; // 5

// methods
// add

// push
numbers.push(60); // [10, 20, 30, 40, 50, 60];
// adds element at the end

// unshift
numbers.unshift(0); // [0, 10, 20, 30, 40, 50, 60];
// adds element from the start

// splice - 
// 1st parameter - index where we want to add element
// 2nd parameter - how many elements we want to delete
// 3rd parameter - element we want to add
numbers.splice(3, 0, 25);
// [0, 10, 20, 25, 30, 40, 50, 60];

// removing elements
// pop
numbers.pop(); // [0, 10, 20, 25, 30, 40, 50]; -> 60
// deletes and returns the last element

// shift
numbers.shift(); // // [10, 20, 25, 30, 40, 50]; -> 0
// deletes and returns element at 0th index

// splice
numbers.splice(2, 2); // [10, 20, 40, 50]; -> [25, 30]
// deletes the no of elements from the specified index
// and returns back an array of all the deleted elements


// map, filter, reduce, foreach -> when we learn callbacks

const integers = [1, 2, 3, 4, 5];
// [1, 4, 9, 16, 25] -> new 
// map
// map takes a callback which defines how to modify each element
const squaredIntegers = integers.map(el => el ** 2);

// [1, 3, 5] -> new
// filter
const oddIntegers = integers.filter(el => el % 2 !== 0);

// 15
// reduce
const addition = integers.reduce((sum, el) => {
    // sum = 15
    // el = 5
    return sum + el
}, 0);

// 1, 2, 3, 4, 5
// foreach -> does NOT return anything
// is ONLY for iteration
integers.forEach((el) => console.log(el))

// destructuring
const laptop = ["Lenovo", 20, "intel"];

// const company = laptop[0];
// const ram = laptop[1];
// const processor = laptop[2];

//    ["Lenovo", 20, "intel"];
const [company, ram, processor] = laptop;

const santoshFriends = ["soham", "mit", "ganesh"];

// assigning myFriends the reference of santoshFriends
const myFriends = santoshFriends;
myFriends.push("ashwini");
// not copying the elements but the reference

// cloning
// slice
const aniruddhaFriends = santoshFriends.slice();
aniruddhaFriends.push('shrinivas');

// spread operator ...
const harishFriends = [...santoshFriends];
harishFriends.push('pooja');

const attendees = [{ name: 'narayn' }, { name: 'ashwini' }];
// creates a SHALLOW CLONE
const attendeesClone = [...attendees];

attendeesClone.push({ name: 'ashvini' });
attendees[0].name = 'narayan';

// assignment -> find out how to create a DEEP CLONE