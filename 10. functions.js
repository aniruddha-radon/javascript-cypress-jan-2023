// block of code which can be invoked
// may accept some input
// may give some output

// traditional function
// function -> keyword
// name of function
// () -> accept a parameter list
// {} -> block/scope of the function inside which the actual code resides
function greet() {
    console.log('this function greets people');
}
// does not accept parameters -> unparameterised
// does not return any value  -> non returning
// registering the function

const ref = greet; // referencing the function
greet(); // invoking/calling/executing function
ref(); // invokes the greet function

// parameterised, non returning
function greetPerson(name) {
    const message = `hello, ${name}`;
    console.log(message);
}

greetPerson(); // allowed in js
// all params will be passed undefined
const value = greetPerson('Aniruddha');
// value has undefined, because greet person returns nothing.
const personToGreet = 'Pooja';
greetPerson(personToGreet);

// unparameterised, returning function
function getGreeting() {
    const message = 'Hello';
    return message;
    // return STOPS the execution of the function

    // this line of code will NEVER be executed
    const secondMessage = 'hi';
}

const greeting = getGreeting();

// parameterised, returning
function createGreeting(name) {
    // name -> Santosh
    const message = `${getGreeting()}, ${name}`;
    // Hello, Santosh
    return message;
}

const finalGreeting = createGreeting('Santosh');
console.log(finalGreeting);

// function expression
const square = function (number) {
    console.log(number ** 2);
}

// arrow functions
// es6
// type of function expressions
const subtract = (n1, n2) => {
    return n1 - n2;
}

const diff = subtract(7, 5);

// omit parameter brackets if you have only 1 parameter
const cube = number => {
    return number ** 3;
}

// get rid of {} block if there is only 1 statement
// and also omit return
// ALWAYS USE ARROW FUNCTIONS
const intoTen = number => number * 10;

const splitName = (name) => {
    const characterArray = name.split('');
    return characterArray;
}

// shorter version 1
const splitNameShortOne = name => {
    const characterArray = name.split('');
    return characterArray;
}

// shorter version 2
const splitNameShortTwo = name => {
    return name.split('');
}

// shorter version 3
const splitNameShortThree = name => name.split('');
// const splitName = name => name.split('');


const x = n => n % 2; // remainder of n by 2
const p = l => l.length; // 
p([1, 2, 3]); // 3
p('aniruddha'); // 9

const w = h => h + h * 2;

// currying
const open = n1 => n2 => n1 * n2;

const result = open(5)(7);




const add = (n1, n2) => n1 + n2;

const difference = (n1, n2) => {
    return n1 - n2;
}

