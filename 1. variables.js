var number; // declaring a variable -> number has undefined
number = 10; // assignment

var primeNumber = 7; // declaration + assignment
// AVOID using var

let square = 25;
let cube;
cube = 125;
// always prefer let over var

const distanceToSun = 123456789;
distanceToSun = 0987654321; // ERROR
// prefer const over let

const PI; // only declaration is not allowed
PI = 3.14;

// verbose variable names
// use nouns
// camelCasing