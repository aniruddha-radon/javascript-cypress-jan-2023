

// Number
const integer = 5;
const decimal = 5.5;
const negative = -10;
const randomNumber = 2345678;

typeof decimal; // number
console.log(typeof decimal);


// String
// a-z, A-Z, 0-9, any symbol
// is enclosed in "", '', ``
const singleQuoted = 'professor oak';
const doubleQuoted = "ash ketchum";
const backTicked = `Pikachu`;

// Boolean
const isRaining = true;
const isSunny = false;

// undefined
const sagar = undefined; // NEVER EVER DO THIS.

// null - pseudo data type
const pooja = null; // NEVER DO THIS.

// collection datatypes
// array -> elements (of ANY datatype) separated by commas, enclosed in []
const numbers = [10, 20, 30, 40, 50];

const person = [
    23, 
    "Aniruddha", "Gohad", 
    false, 
    undefined, 
    null, 
    ["Abhishek", "Pranjali", "Yashshree"],
    { city: "Pune", country: "IN" },
    function() { console.log('blah blah blah') }
];

// object
// key: value pairs, separated by commas,
// enclosed in  {}

const personObj = {
    age: 23,
    name: "Aniruddha",
    surname: "Gohad",
    isMarried: false,
    intelligence: undefined,
    brain: null,
    friends: ["Abhishek", "Pranjali", "Yashshree"],
    address: {
        city: "Pune",
        country: "IN"
    },
    blab: function() {
        console.log('blah blah blah')
    }
};


// function
function greet() {
    console.log('Hiiiii');
}

console.log(typeof greet);



