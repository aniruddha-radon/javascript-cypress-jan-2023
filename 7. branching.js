

if(somecondition) {
    // if somecondition is true
    // then execute this block
}

else if(somecondition) {
    // if somecondition is true
    // then execute this block
}

else {
    // executed when all above conditions are false
}

// else if and else MUST have a if block
// else if and else are NOT compulsory
// if can be standalone
// there can be multiple else if blocks
// but there can be only 1 if and else blocks

if(true) {
    // this is the sole if block
}


// last floor - hard drinks
// second floor - not hard drinks
// first floor - water

const guestAge = 29;


// only one block gets executed
if(guestAge >= 25) {
    console.log('allowed on third floor');
} else if(guestAge >= 18) {
    console.log('allowed on second floor');
} else {
    console.log('allowed on first floor');
}


// switch

const chosenPokemon = 'pikachu';

switch(chosenPokemon) {
    case 'pikachu':
        console.log('pikachu main tumhe chunta hoon');
        break;
    case 'bulbasaur':
        console.log('bulbasaur main tumhe chunta hoon');
        break;
    default:
        console.log('no pokemon to choose');
}

// 