const isLengthFive = arr => arr.length === 5;

// 5, [1, 2, 3, 4, 5]
const searchElement = (searchEl, array) => {
    // searchEl = 6
    // array = [1, 2, 3, 4, 5]

    for(let element of array) {
        // element = 5
        if(element === searchEl) {
            return true;
        }
    }

    return false;
}

const map = (array, modifier) => {
    // array = [1, 2, 3, 4, 5]
    // modifier = x => x ** 2
    const modifiedArray = [];
    // [1, 4, 9, 16, 25]
    for(let el of array) {
        // el = 2
        const modifiedEl = modifier(el);
        // 4
        modifiedArray.push(modifiedEl);
    }

    return modifiedArray;
}

map([1, 2, 3, 4, 5], x => x ** 2);
