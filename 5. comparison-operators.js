// compares LHS with RHS and gives back a boolean

// ==, !=, <, <=, >, >=

9 == 9; // true
8 == 9; // false
9 == 8; // false
'9' == '9'; // true
'a' == 'b'; // false
// only values are checked NOT datatype
'9' == 9; // true
'true' == true; // false

9 != 9; // false
8 != 9; // true
9 != 8; // true
'9' != '9'; // false
'a' != 'b'; // true
// only values are checked NOT datatype
'9' != 9; // false
'true' != true; // true

9 < 9; // false
8 < 9; // true
9 < 8; // false
'9' < '9'; // false
'a' < 'b'; // true
// only values are checked NOT datatype
'9' < 9; // false
'true' < true; // false
'a' < 10000; // false 

9 <= 9; // true
8 <= 9; // true
9 <= 8; // false
'9' <= '9'; // true
'a' <= 'b'; // true
// only values are checked NOT datatype
'9' <= 9; // true
'true' <= true; // false
'a' <= 10000; // false

9 > 9; // false
8 > 9; // false
9 > 8; // true
'9' > '9'; // false
'a' > 'b'; // false
// only values are checked NOT datatype
'9' > 9; // false
'true' > true; // false
'a' > 10000; // false

9 >= 9; // true
8 >= 9; // false
9 >= 8; // true
'9' >= '9'; // true
'a' >= 'b'; // false
// only values are checked NOT datatype
'9' >= 9; // true
'true' >= true; // false
'a' >= 10000; // false


// === (ALWAYS use this over ==)
// checks value AND datatype
9 === 9; // true
'9' === 9; // false

// !== (ALWAYS use this over !=)
8 !== '8'; // true


true === true; // true
false !== true; // true
25 >= 24; // true
false === true; // false
25 > []; // false

true === {}; // false
1 == 1; // true

// COMMON PITFALLS
{} == {}; // false
{} === {}; // false
[1, 2, 3] == [1, 2, 3]; // false
[1, 2, 3] === [1, 2, 3]; // false 