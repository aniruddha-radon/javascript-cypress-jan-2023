// AND, OR, NOT
// combine two booleans to create one

// AND - &&
true && true; // true
true && false; // false
false && true; // false
false && false; // false

// OR - ||
true || true; // true
true || false; // true
false || true; // true
false || false; // false

// NOT !
!true; // false
!false; // true

// TRUTHY & FALSY values
// 0, "", NaN, undefined, null, false -> FALSY
0 && true; // false
// all values except the above are TRUTHY
2 || false; // true