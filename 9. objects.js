// 
const favoriteCharacter = {
    0: "element at 0",
    name: "shanaya",
    age: 24,
    isMarried: false,
    friends: ["guru", "atharva"],
    address: {
        city: "Mumbai",
        country: "IN"
    },
    // after we learn functions
    plan: function() {
        console.log(`${this.name} is planning something`);
    },
    talk: () => {
        console.log(`${this.name} is talking`);
    }
};

favoriteCharacter.name; // shanaya
favoriteCharacter["name"]; // shanaya
favoriteCharacter[0]; // "element at 0"
favoriteCharacter.friends[1]; // atharva

const key = "age";
favoriteCharacter[key]; // 24

favoriteCharacter.key; // undefined

favoriteCharacter.isMarried = true;

favoriteCharacter.weight = 50;
// adds a key "weight" and assigns 50 to it

favoriteCharacter["newKey"] = "some value";

delete favoriteCharacter.newKey; 

// self learn
// Object.keys
// Object.values
// hasOwnProperty